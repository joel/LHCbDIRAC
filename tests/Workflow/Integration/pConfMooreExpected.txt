from ProdConf import ProdConf

ProdConf(
  NOfEvents=-1,
  DDDBTag='Sim08-20130503-1',
  CondDBTag='Sim08-20130503-1-vc-mu100',
  AppVersion='v14r8p1',
  XMLSummaryFile='summaryMoore_00012345_00006789_3.xml',
  Application='Moore',
  OutputFilePrefix='00012345_00006789_3',
  XMLFileCatalog='pool_xml_catalog.xml',
  InputFiles=['LFN:00012345_00006789_2.digi'],
  OutputFileTypes=['digi'],
)