############################################################

"""
   LHCbSystem package

   This contains the LHCb specific plugins for DIRAC.
"""

__RCSID__ = "$Id$"
