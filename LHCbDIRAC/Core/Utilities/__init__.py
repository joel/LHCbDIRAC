############################################################

"""
   LHCbDIRAC.Core.Utilities package

   This contains the LHCb specific Core Utilities.
"""

__RCSID__ = "$Id$"
